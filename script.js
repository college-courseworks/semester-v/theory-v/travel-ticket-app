document
  .getElementById("calculateButton")
  .addEventListener("click", function () {
    const destination = document.getElementById("destination").value;
    const ticketCount = parseInt(document.getElementById("ticketCount").value);
    const isMember = document.getElementById("isMember").checked;

    const ticketPrices = {
      Jakarta: 80_000,
      Bandung: 50_000,
      Surabaya: 200_000,
      Yogyakarta: 150_000,
      Bali: 350_000,
    };

    const ticketPrice = ticketPrices[destination];
    let discount = 0;
    if (isMember) {
      discount = 0.1;
    }

    const totalPrice = ticketPrice * ticketCount * (1 - discount);

    document.getElementById("totalPrice").textContent = totalPrice.toFixed(0);
  });
